=== FEP Group Message ===
Contributors: shamim51
Tags: front end pm,front-end-pm,pm,fep group message
Donate link: https://www.paypal.me/hasanshamim
Requires at least: 4.4
Tested up to: 4.5.3
Stable tag: 1.2

Front End PM Extension for sending group message.

== Description ==
Front End PM Extension for sending group message.

**Features**

* User can send message to pre defined group.
* Admin can create unlimited number of groups.

== Installation ==
1. Upload to the "/wp-content/plugins/" directory.
1. Activate the plugin through the "Plugins" menu in WordPress.
1. Go to Dashboard > front End PM > Settings > Extensions and set as you wish
1. Enjoy

== Frequently Asked Questions ==
= Can i use this plugin to my language? =
Yes. this plugin is translate ready. But If your language is not available you can make one. If you want to help us to translate this plugin to your language you are welcome.


== Screenshots ==

1. Responsive

== Changelog ==

= 1.2 =

* translation added.
* auto updater added.

= 1.1 =

* Initial release.

== Upgrade Notice ==

= 1.2 =

* translation added.
* auto updater added.

= 1.1 =

* Initial release.

