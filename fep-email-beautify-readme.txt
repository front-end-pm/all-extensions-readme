=== FEP Email Beautify ===
Contributors: shamim51
Tags: front end pm,front-end-pm,pm,fep email beautify
Donate link: https://www.paypal.me/hasanshamim
Requires at least: 4.4
Tested up to: 4.5.3
Stable tag: 1.2

Front End PM Extension for email template change from back-end. Also add queue for announcement emails.

== Description ==
Front End PM Extension for email template change from back-end. Also add queue for announcement emails.

**Features**

* Design your email right from dashboard
* Template tag assist you to send unique message to all users.
* Add queue system for announcement email, So you no longer have to worry about your email limit imposed by your hosting.

== Installation ==
1. Upload "front-end-pm" to the "/wp-content/plugins/" directory.
1. Activate the plugin through the "Plugins" menu in WordPress.
1. Go to Dashboard > front End PM > Settings > Emails and set as you wish
1. Enjoy

== Frequently Asked Questions ==
= Can i use this plugin to my language? =
Yes. this plugin is translate ready. But If your language is not available you can make one. If you want to help us to translate this plugin to your language you are welcome.


== Screenshots ==

1. Responsive

== Changelog ==

= 1.2 =

* translation added.
* auto updater added.

= 1.1 =

* Initial release.

== Upgrade Notice ==

= 1.2 =

* translation added.
* auto updater added.

= 1.1 =

* Initial release.

